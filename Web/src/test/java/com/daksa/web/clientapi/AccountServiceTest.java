package com.daksa.web.clientapi;

import com.daksa.web.exception.EndpointException;
import com.daksa.web.infrastructure.TableResult;
import com.daksa.web.infrastructure.endpoint.EndpointRequest;
import com.daksa.web.infrastructure.endpoint.EndpointRequestBuilder;
import com.daksa.web.infrastructure.endpoint.EndpointResponse;
import com.daksa.web.infrastructure.endpoint.EndpointResponseBuilder;
import com.daksa.web.infrastructure.json.Json;
import com.daksa.web.infrastructure.json.JsonResponseReader;
import com.daksa.web.model.AccountModel;
import com.daksa.web.model.UploadModel;
import com.fasterxml.jackson.core.JsonProcessingException;
import org.apache.log4j.BasicConfigurator;
import org.eclipse.jetty.client.HttpClient;
import org.eclipse.jetty.client.api.Request;
import org.eclipse.jetty.http.HttpHeader;
import org.eclipse.jetty.util.ssl.SslContextFactory;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;
import org.mockito.stubbing.OngoingStubbing;

import javax.ws.rs.core.MediaType;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import static org.junit.jupiter.api.Assertions.*;
import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.Mockito.*;

@ExtendWith(MockitoExtension.class)
class AccountServiceTest {

    @InjectMocks
    private GatewayEndpoint gatewayEndpoint;


    @Test
    void getAccounts() throws EndpointException {
        BasicConfigurator.configure();

        HttpClient httpClient = new HttpClient(new SslContextFactory());
        String baseUrl = "http://localhost:8080/core/rest";
        gatewayEndpoint.initialize(httpClient, baseUrl, null);

        EndpointRequest endpointRequest = new EndpointRequestBuilder()
                .method("GET")
                .property("limit", 1)
                .property("offset", 0)
                .property("id", "1")
                .property("name", "Erdi")
                .property("negativeBallStatus", "All")
                .resource("/account")
                .build();

        Request httpRequest = gatewayEndpoint.createHttpRequest(httpClient, endpointRequest);
        Assertions.assertEquals("GET", httpRequest.getMethod());
        Assertions.assertEquals("1", httpRequest.getParams().get("limit").getValue());
        Assertions.assertEquals("0", httpRequest.getParams().get("offset").getValue());
        Assertions.assertEquals("1", httpRequest.getParams().get("id").getValue());
        Assertions.assertEquals("Erdi", httpRequest.getParams().get("name").getValue());

    }

    @Test
    void addACcount() throws EndpointException, JsonProcessingException {
        BasicConfigurator.configure();

        HttpClient httpClient = new HttpClient(new SslContextFactory());
        String baseUrl = "http://localhost:8080/core/rest";
        gatewayEndpoint.initialize(httpClient, baseUrl, null);

        AccountModel accountModel = new AccountModel();
        accountModel.setAccountId("1");

        EndpointRequest endpointRequest = new EndpointRequestBuilder()
                .method("POST")
                .content(Json.getWriter().writeValueAsBytes(accountModel), MediaType.APPLICATION_JSON)
                .resource("/account")
                .build();

        Request httpRequest = gatewayEndpoint.createHttpRequest(httpClient, endpointRequest);
        Assertions.assertEquals("POST", httpRequest.getMethod());

    }

    @Test
    void uploadAccount() throws EndpointException, JsonProcessingException {
        BasicConfigurator.configure();

        HttpClient httpClient = new HttpClient(new SslContextFactory());
        String baseUrl = "http://localhost:8080/core/rest";
        gatewayEndpoint.initialize(httpClient, baseUrl, null);

        UploadModel uploadModel = new UploadModel();
        uploadModel.setAccountId("1");

        EndpointRequest endpointRequest = new EndpointRequestBuilder()
                .method("POST")
                .content(Json.getWriter().writeValueAsBytes(uploadModel), MediaType.APPLICATION_JSON)
                .resource("/account")
                .build();

        Request httpRequest = gatewayEndpoint.createHttpRequest(httpClient, endpointRequest);
        Assertions.assertEquals("POST", httpRequest.getMethod());

    }
}