package com.daksa.web.clientapi;

import com.daksa.web.exception.EndpointException;
import com.daksa.web.infrastructure.endpoint.EndpointRequest;
import com.daksa.web.infrastructure.endpoint.EndpointRequestBuilder;
import com.daksa.web.infrastructure.json.Json;
import com.daksa.web.model.TransferModel;
import com.daksa.web.model.UploadModel;
import com.fasterxml.jackson.core.JsonProcessingException;
import org.apache.log4j.BasicConfigurator;
import org.eclipse.jetty.client.HttpClient;
import org.eclipse.jetty.client.api.Request;
import org.eclipse.jetty.util.ssl.SslContextFactory;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.junit.jupiter.MockitoExtension;

import javax.ws.rs.core.MediaType;

import static org.junit.jupiter.api.Assertions.*;
import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.Mockito.*;

@ExtendWith(MockitoExtension.class)
class TransactionServiceTest {
    @InjectMocks
    private GatewayEndpoint gatewayEndpoint;

    @Test
    void transfer() throws JsonProcessingException, EndpointException {
        BasicConfigurator.configure();

        HttpClient httpClient = new HttpClient(new SslContextFactory());
        String baseUrl = "http://localhost:8080/core/rest";
        gatewayEndpoint.initialize(httpClient, baseUrl, null);

        TransferModel transferModel = new TransferModel();
        transferModel.setSrcAccountId("1");
        transferModel.setDstAccountId("2");

        EndpointRequest endpointRequest = new EndpointRequestBuilder()
                .method("POST")
                .content(Json.getWriter().writeValueAsBytes(transferModel), MediaType.APPLICATION_JSON)
                .resource("/account")
                .build();

        Request httpRequest = gatewayEndpoint.createHttpRequest(httpClient, endpointRequest);
        Assertions.assertEquals("POST", httpRequest.getMethod());
    }

    @Test
    void getTransactionList() throws EndpointException {
        BasicConfigurator.configure();

        HttpClient httpClient = new HttpClient(new SslContextFactory());
        String baseUrl = "http://localhost:8080/core/rest";
        gatewayEndpoint.initialize(httpClient, baseUrl, null);

        EndpointRequest endpointRequest = new EndpointRequestBuilder()
                .method("GET")
                .property("limit", 1)
                .property("offset", 0)
                .resource("/account")
                .build();

        Request httpRequest = gatewayEndpoint.createHttpRequest(httpClient, endpointRequest);
        Assertions.assertEquals("GET", httpRequest.getMethod());
        Assertions.assertEquals("1", httpRequest.getParams().get("limit").getValue());
        Assertions.assertEquals("0", httpRequest.getParams().get("offset").getValue());

    }

    @Test
    void getTransactionDailyReport() throws EndpointException {
        BasicConfigurator.configure();

        HttpClient httpClient = new HttpClient(new SslContextFactory());
        String baseUrl = "http://localhost:8080/core/rest";
        gatewayEndpoint.initialize(httpClient, baseUrl, null);

        EndpointRequest endpointRequest = new EndpointRequestBuilder()
                .method("GET")
                .property("limit", 5)
                .property("offset", 0)
                .resource("/account")
                .build();

        Request httpRequest = gatewayEndpoint.createHttpRequest(httpClient, endpointRequest);
        Assertions.assertEquals("GET", httpRequest.getMethod());
        Assertions.assertEquals("5", httpRequest.getParams().get("limit").getValue());
        Assertions.assertEquals("0", httpRequest.getParams().get("offset").getValue());
    }
}