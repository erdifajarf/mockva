package com.daksa.web.infrastructure.endpoint;

import java.util.Map;

/**
 *
 * @author Ginan
 */
public interface EndpointRequest {
	
	String getRequestId();
	
	String getCorrelationId();
	
	String getResource();
			
	String getMethod();
	
	Map<String, Object> getProperties();
	
	byte[] getContent();
	
	String getContentType();
}
