//package com.daksa.endpoint;
//
//
//import java.nio.ByteBuffer;
//import java.text.SimpleDateFormat;
//import java.util.Date;
//import java.util.Locale;
//import java.util.Properties;
//
//import com.daksa.exception.EndpointException;
//import org.apache.commons.codec.binary.Base64;
//import org.apache.commons.codec.digest.DigestUtils;
//import org.apache.commons.codec.digest.HmacUtils;
//import org.eclipse.jetty.client.HttpClient;
//import org.eclipse.jetty.client.api.Request;
//import org.eclipse.jetty.http.HttpHeader;
//import org.slf4j.Logger;
//import org.slf4j.LoggerFactory;
//
///**
// *
// * @author Ginan
// */
//public class DA01RestEndpoint extends RestEndpoint {
//
//	private static final long serialVersionUID = 1L;
//	private static final Logger LOG = LoggerFactory.getLogger(DA01RestEndpoint.class);
//
//	public static final String PROP_API_KEY = "apiKey";
//	public static final String PROP_SECRET_KEY = "secretKey";
//
//	private String apiKey;
//	private String secretKey;
//
//	@Override
//	public void initialize(HttpClient client, String baseUrl, Properties properties) {
//		super.initialize(client, baseUrl, properties);
//		this.apiKey = properties.getProperty(PROP_API_KEY);
//		this.secretKey = properties.getProperty(PROP_SECRET_KEY);
//	}
//
//	@Override
//	protected Request createHttpRequest(HttpClient client, EndpointRequest endpointRequest) throws EndpointException {
//		Request request = super.createHttpRequest(client, endpointRequest);
//		createSignature(request, endpointRequest.getResource());
//		return request;
//	}
//
//	private void createSignature(Request request, String path) {
//		SimpleDateFormat dateFormat = new SimpleDateFormat("EEE, dd MMM yyyy HH:mm:ss z", Locale.US);
//		request.getHeaders().add(HttpHeader.DATE, dateFormat.format(new Date()));
//		StringBuilder digestBuilder = new StringBuilder(request.getMethod()).append("\n");
//		if (request.getContent() != null) {
//			byte[] content = new byte[(int) request.getContent().getLength()];
//			request.getContent().forEach((ByteBuffer t) -> {
//				t.get(content);
//			});
//			digestBuilder.append(DigestUtils.md5Hex(content)).append("\n")
//					.append(request.getHeaders().get(HttpHeader.CONTENT_TYPE)).append("\n");
//		}
//		digestBuilder.append(request.getHeaders().get(HttpHeader.DATE)).append("\n")
//				.append(path);
//		String digest = digestBuilder.toString();
//		String signature = "DA01 " + apiKey + ":" + Base64.encodeBase64String(HmacUtils.hmacSha1(secretKey, digest));
//		request.header(HttpHeader.AUTHORIZATION, signature);
//	}
//}
