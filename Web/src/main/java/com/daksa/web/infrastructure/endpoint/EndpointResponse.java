package com.daksa.web.infrastructure.endpoint;

import java.util.Map;

/**
 *
 * @author Ginan
 */
public interface EndpointResponse {

	String getRequestId();
	
	String getCorrelationId();
	
	Map<String, Object> getProperties();
	
	byte[] getContent();
	
	String getContentType();
}
