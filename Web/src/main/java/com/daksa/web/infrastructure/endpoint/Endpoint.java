package com.daksa.web.infrastructure.endpoint;


import com.daksa.web.exception.EndpointException;

import java.io.Serializable;

/**
 *
 * @author Ginan
 */
public interface Endpoint extends Serializable {
	
	EndpointResponse send(EndpointRequest request) throws EndpointException;

	String getHostId();
}
