package com.daksa.web.infrastructure.endpoint;

import com.daksa.web.infrastructure.json.Json;
import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.JavaType;

import java.io.IOException;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import org.apache.commons.lang3.ArrayUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 *
 * @author Ginan <ginanjarpramadita@gmail.com>
 */
public class EndpointResponseReader {

    private static final Logger LOG = LoggerFactory.getLogger(EndpointResponseReader.class);

    private final EndpointResponse response;

    public EndpointResponseReader(EndpointResponse response) {
        this.response = response;
    }

    public <T> T getContentObject(Class<T> contentClass) {
        if (ArrayUtils.isNotEmpty(response.getContent())) {
            try {
                T content = Json.getReader().forType(contentClass).readValue(response.getContent());
                return content;
            } catch (IOException e) {
                throw new RuntimeException(e);
            }
        } else {
            return null;
        }
    }

    public <T> List<T> getContentList(Class<T> contentClass) {
        if (ArrayUtils.isNotEmpty(response.getContent())) {
            try {
                JavaType javaType = Json.getInstance()
                        .getObjectMapper()
                        .getTypeFactory()
                        .constructParametricType(List.class, contentClass);
                List<T> content = Json.getReader().forType(javaType).readValue(response.getContent());
                return content;
            } catch (IOException e) {
                throw new RuntimeException(e);
            }
        } else {
            return null;
        }
    }

    public Map<String, Object> getContentAsMap() {
        if (ArrayUtils.isNotEmpty(response.getContent())) {
            try {
                String json = new String(response.getContent());
                Map<String, Object> content = Json.getReader()
                        .forType(new TypeReference<HashMap<String, Object>>() {
                        })
                        .readValue(json);
                return content;
            } catch (IOException e) {
                throw new RuntimeException(e);
            }
        } else {
            return null;
        }
    }

    public String getContentAsString() {
        if (ArrayUtils.isNotEmpty(response.getContent())) {
           String responseString = new String(response.getContent());
           return responseString;
        } else {
            return null;
        }
    }

}
