package com.daksa.web.infrastructure;

import org.eclipse.jetty.client.HttpClient;
import org.eclipse.jetty.util.ssl.SslContextFactory;

import javax.annotation.PostConstruct;
import javax.annotation.PreDestroy;
import javax.enterprise.context.ApplicationScoped;
import javax.enterprise.inject.Produces;

@ApplicationScoped
public class HttpClientProducer {

    private HttpClient client;

    @PostConstruct
    public void postConstruct() {
        try {
            client = new HttpClient(new SslContextFactory());
            client.start();
        } catch (Exception ex) {
            throw new RuntimeException(ex);
        }
    }

    @PreDestroy
    public void preDestroy() {
        try {
            client.stop();
        } catch (Exception ex) {
            throw new RuntimeException(ex);
        }
    }

    @Produces
    public HttpClient getHttpClient() {
        return client;
    }
}
