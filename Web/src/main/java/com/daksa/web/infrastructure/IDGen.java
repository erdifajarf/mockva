package com.daksa.web.infrastructure;

import java.util.UUID;

/**
 *
 * @author Ginan
 */
public class IDGen {
	
	public static String generate() {
		return UUID.randomUUID().toString().replace("-", "");
	}
}
