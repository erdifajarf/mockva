package com.daksa.web.exception;

/**
 *
 * @author Nafhul A
 */
public class NotExistsException extends RestException {

	public NotExistsException(String message) {
		super(400, "E1", message);
	}

	public NotExistsException() {
		super(400, "E1", "Not Exists");
	}

}
