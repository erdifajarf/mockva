package com.daksa.web.exception;

/**
 *
 * @author Nafhul A
 */
public class InvalidTokenException extends RestException {

	public InvalidTokenException(String message) {
		super(400, "IT", message);
	}

	public InvalidTokenException() {
		super(400, "IT", "Invalid Token");
	}
}
