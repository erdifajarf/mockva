package com.daksa.web.clientapi;

import com.daksa.web.infrastructure.TableResult;
import com.daksa.web.infrastructure.endpoint.EndpointRequestBuilder;
import com.daksa.web.exception.EndpointException;
import com.daksa.web.infrastructure.json.Json;
import com.daksa.web.infrastructure.json.JsonResponseReader;
import com.daksa.web.model.AccountModel;
import com.daksa.web.model.DailyTransactionModel;
import com.daksa.web.model.TransactionModel;
//import com.daksa.web.model.TransferModel;
import com.daksa.web.model.TransferModel;
import com.fasterxml.jackson.core.JsonProcessingException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.enterprise.context.Dependent;
import javax.inject.Inject;
import javax.persistence.EntityManager;
import javax.ws.rs.core.MediaType;
import java.io.Serializable;
import java.math.BigDecimal;
import java.util.List;
import java.util.Map;

@Dependent
public class TransactionService implements Serializable {

    public static final long serialVersionUID=1L;
    private static final Logger LOG = LoggerFactory.getLogger(TransactionService.class);

    @Inject
    private GatewayEndpoint endpoint;

    public void transfer(TransferModel transferModel) throws JsonProcessingException, EndpointException{
        endpoint.send(new EndpointRequestBuilder()
                .method("POST")
                .content(Json.getWriter().writeValueAsBytes(transferModel), MediaType.APPLICATION_JSON)
                .resource("/transaction/transfer")
                .build());
    }

    public TableResult<TransactionModel> getTransactionList(Map<String,Object> filter) throws JsonProcessingException, EndpointException{
            TableResult<TransactionModel> transaction = new JsonResponseReader(endpoint.send(new EndpointRequestBuilder()
                    .method("GET")
                    .property("limit", filter.get("limit"))
                    .property("offset", filter.get("offset"))
                    .resource("/transaction")
                    .build())).getContentTable(TransactionModel.class);

            return transaction;
    }


    public List<DailyTransactionModel> getTransactionDailyReport() throws JsonProcessingException, EndpointException{
        TableResult<DailyTransactionModel> dailyTransaction = new JsonResponseReader(endpoint.send(new EndpointRequestBuilder()
                .method("GET")
                .resource("/transaction/daily")
                .build())).getContentTable(DailyTransactionModel.class);
        return dailyTransaction.getData();
    }


}