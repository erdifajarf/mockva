package com.daksa.web.clientapi;

import com.daksa.web.infrastructure.TableResult;
import com.daksa.web.infrastructure.endpoint.EndpointRequestBuilder;
import com.daksa.web.exception.EndpointException;
import com.daksa.web.infrastructure.json.Json;
import com.daksa.web.infrastructure.json.JsonResponseReader;
import com.daksa.web.model.AccountModel;
import com.daksa.web.model.UploadModel;
import com.fasterxml.jackson.core.JsonProcessingException;

import javax.enterprise.context.Dependent;
import javax.inject.Inject;
import javax.ws.rs.core.MediaType;
import java.io.InputStream;
import java.io.Serializable;
import java.util.List;
import java.util.Locale;
import java.util.Map;

@Dependent
public class AccountService implements Serializable {

    public static final long serialVersionUID=1L;
//    private static final Logger LOG = LoggerFactory.getLogger(AccountService.class);

    @Inject
    private GatewayEndpoint endpoint;

    public TableResult<AccountModel> getAccounts(Map<String,Object> filter) throws JsonProcessingException, EndpointException{
        TableResult<AccountModel> account = new JsonResponseReader(endpoint.send(new EndpointRequestBuilder()
                .method("GET")
                .property("limit", filter.get("limit"))
                .property("offset", filter.get("offset"))
                .property("id", filter.get("id"))
                .property("name", filter.get("name"))
                .property("negativeBallStatus", filter.get("negativeBallStatus"))
                .resource("/account")
                .build())).getContentTable(AccountModel.class);
        return account;
    }


    public void addACcount(AccountModel model) throws JsonProcessingException, EndpointException{
       endpoint.send(new EndpointRequestBuilder()
                .method("POST")
                .content(Json.getWriter().writeValueAsBytes(model), MediaType.APPLICATION_JSON)
                .resource("/account")
                .build());
    }

    public void uploadAccount(UploadModel uploadModel) throws JsonProcessingException, EndpointException{
        endpoint.send(new EndpointRequestBuilder()
                .method("POST")
                .content(Json.getWriter().writeValueAsBytes(uploadModel), MediaType.APPLICATION_JSON)
                .resource("/account/upload")
                .build());
    }
}
