package com.daksa.web.clientapi;


import com.daksa.web.exception.EndpointException;
import com.daksa.web.infrastructure.endpoint.EndpointRequest;
import org.eclipse.jetty.client.HttpClient;

import javax.annotation.PostConstruct;
import javax.faces.bean.ApplicationScoped;

import com.daksa.web.infrastructure.endpoint.RestEndpoint;
import org.eclipse.jetty.client.api.Request;

import javax.inject.Inject;

@ApplicationScoped
public class GatewayEndpoint extends RestEndpoint {
    private static final long serialVersionUID = 1L;

    @Inject
    private HttpClient httpClient;
//    @Inject
//    private ClientapiConfig config;

    @PostConstruct
    public void initialize(){
        super.initialize(httpClient,"http://localhost:8080/core/rest",null);
    }

    @Override
    public Request createHttpRequest(HttpClient client, EndpointRequest endpointRequest) throws EndpointException{
        return super.createHttpRequest(client,endpointRequest);
    }
}
