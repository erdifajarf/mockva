package com.daksa.web.ui.vaadin;

import java.io.*;
import java.math.BigDecimal;
import java.nio.charset.Charset;
import java.nio.charset.StandardCharsets;
import java.sql.Time;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.time.Instant;
import java.util.*;

import com.daksa.web.clientapi.AccountService;
import com.daksa.web.clientapi.TransactionService;
import com.daksa.web.exception.EndpointException;
import com.daksa.web.model.*;


import com.fasterxml.jackson.core.JsonProcessingException;
import com.helger.commons.compare.ESortOrder;
import com.vaadin.cdi.annotation.RouteScoped;


import com.vaadin.event.dd.acceptcriteria.Not;
import com.vaadin.flow.component.Component;
import com.vaadin.flow.component.Text;
import com.vaadin.flow.component.charts.Chart;
import com.vaadin.flow.component.charts.model.*;
import com.vaadin.flow.component.combobox.ComboBox;
import com.vaadin.flow.component.dependency.CssImport;
import com.vaadin.flow.component.dependency.StyleSheet;
import com.vaadin.flow.component.dialog.Dialog;
import com.vaadin.flow.component.formlayout.FormLayout;
import com.vaadin.flow.component.grid.Grid;
import com.vaadin.flow.component.grid.GridVariant;
import com.vaadin.flow.component.html.Div;
import com.vaadin.flow.component.html.Label;
import com.vaadin.flow.component.html.Paragraph;
import com.vaadin.flow.component.html.Span;
import com.vaadin.flow.component.notification.Notification;
import com.vaadin.flow.component.page.Push;
import com.vaadin.flow.component.textfield.*;
import com.vaadin.flow.data.binder.ReadOnlyHasValue;
import com.vaadin.flow.data.binder.ValidationException;
import com.vaadin.flow.data.converter.StringToBigDecimalConverter;
import com.vaadin.flow.data.provider.ConfigurableFilterDataProvider;
import com.vaadin.flow.data.provider.DataProvider;
import com.vaadin.flow.data.provider.Query;
import com.vaadin.flow.data.provider.QuerySortOrder;
import com.vaadin.flow.data.value.ValueChangeMode;
import com.vaadin.flow.router.Route;
import com.vaadin.flow.component.button.Button;
import com.vaadin.flow.router.PageTitle;
import com.vaadin.flow.component.datepicker.DatePicker;
import com.vaadin.flow.component.orderedlayout.VerticalLayout;
import com.vaadin.flow.component.polymertemplate.Id;
import com.vaadin.flow.component.radiobutton.RadioButtonGroup;
import com.vaadin.flow.component.upload.Upload;
import com.vaadin.flow.component.upload.receivers.MemoryBuffer;
import com.vaadin.flow.data.binder.Binder;
import com.vaadin.flow.data.converter.LocalDateToDateConverter;
import com.vaadin.flow.shared.communication.PushMode;
import com.vaadin.server.ErrorMessage;
import com.vaadin.server.Page;
import org.apache.commons.collections.map.HashedMap;


import javax.annotation.PostConstruct;
import javax.inject.Inject;

import static com.helger.commons.mock.CommonsAssert.assertEquals;
import static org.apache.commons.lang3.RandomStringUtils.randomAlphabetic;


/**
 * @author Muhammad Rizki
 */

@RouteScoped
@Route(value = "vaadin")
@StyleSheet("./styles/main.css")


@CssImport(value="./styles/vaadin-text-field-styles.css", themeFor = "vaadin-text-field")
@PageTitle("Mockva")

public class HomePage extends Div {
    private Binder<AccountModel> binder;
    private Binder<AccountModel> binderForName;
    private Binder<TransferModel> binderTransfer;
    private VerticalLayout layRegist, layUpload, layGridData, layGridTransaction, layChart, layDialog;
    @Id("id")
    private TextField id;
    @Id("name")
    private TextField name;
    @Id("address")
    private TextField address;
    @Id("date")
    private DatePicker date;
    @Id("ballance")
    private BigDecimalField ballance;
    @Id("negBal")
    private RadioButtonGroup<Boolean> negBal;
    @Id("srcAccountId")
    private TextArea srcAccountId;
    @Id("srcAccountName")
    private TextArea srcAccountName;
    @Id("dstAccountId")
    private TextField dstAccountId;
    @Id("dstAccountId")
    private TextArea dstAccountName;
    @Id("amount")
    private TextField amount;
    private Button submitButton, uploadButton;
    private List<AccountModel> accountList;
    private List<TransactionModel> transactionList;
    private Grid<AccountModel> gridData = new Grid<>();
    private Grid<TransactionModel> gridTransaction;
    @Inject
    private AccountService accountService;
    @Inject
    private TransactionService transactionService;
    private List<AccountModel> selectedAccount;
    private TextField filterId, filterName;
    private ComboBox<String> filterNegatifBall;
    private String tempId, tempName, tempNegativeBall = "All";

    Chart chart = new Chart(ChartType.LINE);
    Configuration configuration = chart.getConfiguration();
    DataSeries dataSeries = new DataSeries();

    YAxis yAxis = configuration.getyAxis();
    XAxis xAxis = configuration.getxAxis();
    DataLabels callout = new DataLabels(true);


    DataSeriesItem dataSeriesItem = new DataSeriesItem();

    public HomePage() throws Exception {
//        addClassName("container");
    }

    @PostConstruct
    public void initiate() throws Exception {
        createFormRegist();
        createChart();
        createUploadBatch();
        createGridData();
        createGridTransaction();

    }

    public void createFormRegist() {
        //Layout 1
        Div content2 = new Div();
        content2.addClassName("formPanel");
        add(content2);
        this.layRegist = new VerticalLayout();

        this.id = new TextField();
        this.id.addClassName("inputId");
        this.id.setLabel("Id");

        this.name = new TextField();
        this.name.addClassName("inputName");
        this.name.setLabel("Name");

        this.address = new TextField();
        this.address.addClassName("inputAddress");
        this.address.setLabel("Address");

        this.date = new DatePicker();
        this.date.addClassName("inputDate");
        this.date.setLabel("Date");

        this.negBal = new RadioButtonGroup<>();
        this.negBal.addClassName("inputNegBall");
        this.negBal.setLabel("Allow negative ballance");
        this.negBal.setItems(Boolean.TRUE, Boolean.FALSE);
        this.negBal.setValue(false);
        this.negBal.setRequired(true);


        this.binder = new Binder<>(AccountModel.class);

        this.binder.forField(this.id).withValidator(id -> id.length() < 17,
                "ID tidak boleh melebihi 16 karakter").asRequired()
                .bind(AccountModel::getAccountId, AccountModel::setAccountId);

        this.binder.forField(this.name).withValidator(name -> name.length() < 65,
                "Nama tidak boleh melebihi 64 karakter").asRequired()
                .bind(AccountModel::getName, AccountModel::setName);

        this.binder.forField(this.address).withValidator(address -> address.length() < 256,
                "Alamat tidak boleh melebihi 255 karakter")
                .bind(AccountModel::getAddress, AccountModel::setAddress);

        this.binder.forField(this.date)
                .withConverter(new LocalDateToDateConverter()).asRequired()
                .bind(AccountModel::getBirthdate, AccountModel::setBirthdate);

        this.binder.forField(this.negBal).asRequired()
                .bind(AccountModel::getAllowNegBall, AccountModel::setAllowNegBall);

        this.submitButton = new Button("Submit",
                event -> {
                    try {
                        AccountModel accountModel = new AccountModel();
                        binder.writeBean(accountModel);
                        accountService.addACcount(accountModel);
                        refreshAll();
                        this.id.setValue("");
                        this.name.setValue("");
                        this.date.setValue(null);
                        this.address.setValue("");

                        this.binder.forField(this.id);
                        this.binder.forField(this.name);
                        this.binder.forField(this.date);
                        this.binder.forField(this.address);


                        Notification.show("Registration success",
                                4000, Notification.Position.MIDDLE);

                    } catch (ValidationException e) {
                        Notification.show("Fill required field",
                                2000, Notification.Position.BOTTOM_CENTER);

                        this.id.setErrorMessage("Id cannot be empty");
                        this.name.setErrorMessage("Name cannot be empty");
                        this.date.setErrorMessage("Birthdate cannot be empty");
                    } catch (EndpointException e) {
                        e.printStackTrace();
                        Notification.show("Id has been used",
                                2000, Notification.Position.BOTTOM_CENTER);
                    } catch (JsonProcessingException e) {
                        e.printStackTrace();
                        Notification.show("Id has been used",
                                2000, Notification.Position.BOTTOM_CENTER);
                    }
                });
        this.submitButton.addThemeName("tombol");

        Label titleForm = new Label("Registration Form");
        titleForm.addClassName("regisTitle");
        this.layRegist.add(titleForm,this.id, this.name, this.address, this.date, this.negBal, this.submitButton);
        content2.add(this.layRegist, this.submitButton);
    }

    public void createTransferDialog(AccountModel accountModel) throws ValidationException, EndpointException, JsonProcessingException {
        Dialog dialog = new Dialog();
        dialog.setWidth("55%");
        dialog.setHeight("50%");

        this.layDialog = new VerticalLayout();

        this.srcAccountId = new TextArea();
        this.srcAccountId.setLabel("Account Src Id");
        this.srcAccountId.setWidth("100%");
        this.srcAccountId.setReadOnly(true);
        this.srcAccountId.setValue(accountModel.getAccountId());

        this.srcAccountName = new TextArea();
        this.srcAccountName.setLabel("Account Src Name");
        this.srcAccountName.setWidth("100%");
        this.srcAccountName.setReadOnly(true);
        this.srcAccountName.setValue(accountModel.getName());

        this.dstAccountId = new TextField();
        this.dstAccountId.setLabel("Account Dst Id");
        this.dstAccountId.setWidth("100%");
        this.dstAccountId.setValueChangeMode(ValueChangeMode.EAGER);

        this.dstAccountName = new TextArea();
        this.dstAccountName.setLabel("Account Dst Name");
        this.dstAccountName.setWidth("100%");
        this.dstAccountName.setReadOnly(true);

        this.amount = new TextField();
        this.amount.setLabel("Amount");
        this.amount.setWidth("100%");

        this.binderTransfer = new Binder<>(TransferModel.class);
        this.binderTransfer.forField(dstAccountId).asRequired().
                bind(TransferModel::getDstAccountId, TransferModel::setDstAccountId);

        this.binderTransfer.forField(amount).asRequired().
                withConverter(new StringToBigDecimalConverter("Amount harus berupa angka")).
                bind(TransferModel::getAmount, TransferModel::setAmount);

        this.dstAccountId.addValueChangeListener(event -> {
            try {
                Map<String, Object> filter = new HashMap<>();
                filter.put("limit", 0);
                filter.put("offset", 0);
                filter.put("id", event.getValue());
                filter.put("name", null);
                filter.put("negativeBallStatus", null);

                List<AccountModel> result = accountService.getAccounts(filter).getData();
                if (result.size() == 0) {
                    this.dstAccountName.setValue("");
                } else {
                    this.dstAccountName.setValue(result.get(0).getName());
                }
            } catch (JsonProcessingException e) {
                e.printStackTrace();
            } catch (EndpointException e) {
                e.printStackTrace();
            }
        });

        Button transferButton = new Button("Transfer",
                event -> {
                    try {
                        TransferModel transferModel = new TransferModel();
                        transferModel.setSrcAccountId(this.srcAccountId.getValue());

                        this.binderTransfer.writeBean(transferModel);

                        transactionService.transfer(transferModel);
                        dialog.close();
                        refreshAllTransaction();
                        refreshAll();
                        updateDataSeries();


                    } catch (EndpointException e) {
                        Notification.show("This account not allow for negative ballance and amount must greater than 0",
                                3000, Notification.Position.BOTTOM_CENTER);
                    } catch (JsonProcessingException e) {

                    } catch (ValidationException e) {
                        Notification.show("Fill destination ID and Amount",
                                3000, Notification.Position.BOTTOM_CENTER);
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                });

        this.layDialog.add(this.srcAccountId, this.srcAccountName, this.dstAccountId,
                this.dstAccountName, amount, transferButton);
        dialog.add(this.layDialog);
        dialog.open();

    }

    public Button createTransferButton(AccountModel accountModel) {
        @SuppressWarnings("unchecked")
        Button transferButton = new Button("Transfer");
        transferButton.addClassName("transferButtonGrid");
        transferButton.addClickListener(event -> {

            try {
                createTransferDialog(accountModel);
            } catch (ValidationException validationException) {
                validationException.printStackTrace();
            } catch (EndpointException e) {
                e.printStackTrace();
            } catch (JsonProcessingException e) {
                e.printStackTrace();
            }
        });

        return transferButton;
    }

    public void createUploadBatch() {
        Div content3 = new Div();
        content3.addClassName("uploadPanel");
        add(content3);

        MemoryBuffer buffer = new MemoryBuffer();
        Upload upload = new Upload(buffer);


        upload.addSucceededListener(event -> {
            InputStream inputStream = buffer.getInputStream();
            String resultOfInputStream = "";
            try {
                resultOfInputStream = new String(inputStream.readAllBytes());
            } catch (IOException e) {
                e.printStackTrace();
            }


            UploadModel uploadModel = new UploadModel();
            uploadModel.setInputStream(resultOfInputStream);

            try {
                accountService.uploadAccount(uploadModel);

                refreshAll();
            } catch (JsonProcessingException e) {
                e.printStackTrace();
            } catch (EndpointException e) {
                e.printStackTrace();
            }

        });

        this.layUpload = new VerticalLayout();
        this.layUpload.add(upload);
        content3.add(this.layUpload);
        content3.add(upload);
    }

    public void createGridData() throws Exception {
        Div content4 = new Div();
        content4.addClassName("gridDataPanel");
        add(content4);

        this.layGridData = new VerticalLayout();
        this.layGridData.addClassName("layoutGridData");
        this.gridData = new Grid<AccountModel>();
        this.gridData.addClassName("gridData");


        gridData.setPageSize(20);
        gridData.setSelectionMode(Grid.SelectionMode.NONE);
        DataProvider<AccountModel, Map<String, Object>> dataProvider = DataProvider.fromFilteringCallbacks(
                query -> {
                    try {
                        List<AccountModel> result = fetchData(createFilterParam(query));
                        return result.stream();

                    } catch (Exception e) {
                        throw new RuntimeException(e);
                    }
                },
                query -> {
                    try {
                        int result = countData(createFilterParam(query));
                        return result;
                    } catch (Exception e) {
                        throw new RuntimeException(e);
                    }
                }
        );

        ConfigurableFilterDataProvider filterDataProvider = dataProvider.withConfigurableFilter(
                (Void v, Map<String, Object> filterOutput) -> filterOutput);
        gridData.setDataProvider(filterDataProvider);


        filterId = new TextField();
        filterId.addClassName("filterId");
        filterId.setValueChangeMode(ValueChangeMode.EAGER);
        filterId.setPlaceholder("Find by id..");
        filterId.setClearButtonVisible(true);

        filterId.addValueChangeListener(event -> {
            tempId = event.getValue();

            Map<String, Object> filter = new HashMap<>();
            filter.put("limit", 0);
            filter.put("offset", 0);
            filter.put("id", tempId);
            filter.put("name", tempName);
            filter.put("negativeBallStatus", tempNegativeBall);

            List<AccountModel> resultId = null;
            try {
                resultId = accountService.getAccounts(filter).getData();
            } catch (JsonProcessingException e) {
                e.printStackTrace();
            } catch (EndpointException e) {
                e.printStackTrace();
            }
            gridData.setItems(resultId);
        });


        filterName = new TextField();
        filterName.addClassName("filterName");
        filterName.setValueChangeMode(ValueChangeMode.EAGER);
        filterName.setPlaceholder("Find by name..");
        filterName.setClearButtonVisible(true);

        filterName.addValueChangeListener(event -> {
            try {
                tempName = event.getValue();
                Map<String, Object> filter = new HashMap<>();
                filter.put("limit", 0);
                filter.put("offset", 0);
                filter.put("id", tempId);
                filter.put("name", tempName);
                filter.put("negativeBallStatus", tempNegativeBall);
                List<AccountModel> resultId = accountService.getAccounts(filter).getData();
                gridData.setItems(resultId);
                if (resultId.size() == 0) {
                    Notification.show("Name: " + event.getValue() + " not found",
                            3000, Notification.Position.BOTTOM_CENTER);
                }
            } catch (JsonProcessingException jsonProcessingException) {
                jsonProcessingException.printStackTrace();
            } catch (EndpointException endpointException) {
                endpointException.printStackTrace();
            }
        });


        filterNegatifBall = new ComboBox<>();
        filterNegatifBall.addClassName("filterNegBall");
        filterNegatifBall.setItems("All", "true", "false");
        filterNegatifBall.setValue("All");

        filterNegatifBall.addValueChangeListener(event -> {
            try {
                tempNegativeBall = event.getValue();
                Map<String, Object> filter = new HashMap<>();
                filter.put("limit", 0);
                filter.put("offset", 0);
                filter.put("id", null);
                filter.put("name", null);
                filter.put("negativeBallStatus", tempNegativeBall);
                List<AccountModel> resultNegBall = accountService.getAccounts(filter).getData();

                gridData.setItems(resultNegBall);
            } catch (JsonProcessingException jsonProcessingException) {
                jsonProcessingException.printStackTrace();
            } catch (EndpointException endpointException) {
                endpointException.printStackTrace();
            }
        });

        gridData.addColumn(AccountModel::getAccountId).setHeader("Id").setSortable(true);
        gridData.addColumn(AccountModel::getName).setHeader("Name").setSortable(true);
        gridData.addColumn(AccountModel::getBalance).setHeader("Balance").setSortable(true);
        gridData.addColumn(AccountModel::getAllowNegBall).setHeader("Allow Negative Balance").setSortable(true);

        gridData.addThemeVariants(GridVariant.LUMO_WRAP_CELL_CONTENT);
        gridData.addThemeVariants(GridVariant.LUMO_ROW_STRIPES);

        selectedAccount = new ArrayList<>();

        gridData.addComponentColumn(this::createTransferButton);
        gridData.setSelectionMode(Grid.SelectionMode.NONE);

        Label titleAccount = new Label("Account List");
        titleAccount.addClassName("accountTitle");
        this.layGridData.add(gridData);
        content4.add(filterId, filterName, filterNegatifBall, this.layGridData, this.gridData);

    }

    public void createChart() throws Exception {
        this.layChart = new VerticalLayout();

        Div content6 = new Div();
        content6.addClassName("chartPanel");
        add(content6);
        chart.addClassName("chart");
        configuration.setTitle("Transaction Chart");

        yAxis.setTitle("Amount");
        xAxis.setTitle("Transaction Date");


        configuration.getLegend().setEnabled(false);
//        Legend legend = configuration.getLegend();
//        legend.setLayout(LayoutDirection.VERTICAL);
//        legend.setVerticalAlign(VerticalAlign.MIDDLE);
//        legend.setAlign(HorizontalAlign.RIGHT);

//        PlotOptionsSeries plotOptionsSeries = new PlotOptionsSeries();
//        configuration.setPlotOptions(plotOptionsSeries);

        List<DailyTransactionModel> allTransaction = transactionService.getTransactionDailyReport();
        callout.setShape(Shape.CALLOUT);

        int idx = 0;
        DateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd");
        for (DailyTransactionModel item : allTransaction) {
            dataSeriesItem = new DataSeriesItem(dateFormat.format(item.getTransactionDate()), item.getTotalAmount());
            dataSeries.add(dataSeriesItem);

            dataSeries.get(idx).setDataLabels(callout);
            xAxis.addCategory(dateFormat.format(item.getTransactionDate()));
            idx++;
        }
        configuration.setSeries(dataSeries);

        this.layChart.add(chart);
        content6.add(this.layChart);

    }

    public void updateDataSeries() throws EndpointException, JsonProcessingException {
        List<DailyTransactionModel> allTransaction = transactionService.getTransactionDailyReport();
        callout.setShape(Shape.CALLOUT);

        DateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd");

        List<DataSeriesItem> tempDataSeries = dataSeries.getData();

        if(dataSeries.size()>0){
            dataSeries.remove(tempDataSeries.get(dataSeries.size() - 1));
        }


        String[] tempXCategories = xAxis.getCategories();
        if(tempXCategories.length > 1){
            xAxis.removeCategory(tempXCategories[tempXCategories.length - 1]);
        }


        int lastDataSeriesIndex = allTransaction.size() - 1;

        DataSeriesItem newDataSeriesItem = new DataSeriesItem(dateFormat.format(allTransaction.get(lastDataSeriesIndex).getTransactionDate()), allTransaction.get(lastDataSeriesIndex).getTotalAmount());
        dataSeries.add(newDataSeriesItem);
        dataSeries.get(lastDataSeriesIndex).setDataLabels(callout);
        xAxis.addCategory(dateFormat.format(allTransaction.get(lastDataSeriesIndex).getTransactionDate()));


        chart.drawChart();

    }

    public void createGridTransaction() throws Exception {
        Div content5 = new Div();
        content5.addClassName("gridTransactionPanel");
        add(content5);

        this.layGridTransaction = new VerticalLayout();
        this.gridTransaction = new Grid<TransactionModel>();

        gridTransaction.setPageSize(20);
        gridTransaction.setSelectionMode(Grid.SelectionMode.NONE);
        DataProvider<TransactionModel, Map<String, Object>> dataProvider = DataProvider.fromFilteringCallbacks(
                query -> {
                    try {
                        List<TransactionModel> result = fetchTransactionData(createFilterParam(query));
                        return result.stream();
                    } catch (Exception e) {
                        throw new RuntimeException(e);
                    }
                },
                query -> {
                    try {
                        int result = countTransactionData(createFilterParam(query));
                        return result;
                    } catch (Exception e) {
                        throw new RuntimeException(e);
                    }
                }
        );
        ConfigurableFilterDataProvider filterDataProvider = dataProvider.withConfigurableFilter(
                (Void v, Map<String, Object> filterOutput) -> filterOutput);
        gridTransaction.setDataProvider(filterDataProvider);


        gridTransaction.addColumn(TransactionModel::getTransactionId).setHeader("Id");
        gridTransaction.addColumn(TransactionModel::getSrcAccountId).setHeader("Person SRC");
        gridTransaction.addColumn(TransactionModel::getDstAccountId).setHeader("Person DST");
        gridTransaction.addColumn(TransactionModel::getAmount).setHeader("Amount");

        gridTransaction.addThemeVariants(GridVariant.LUMO_WRAP_CELL_CONTENT);
        gridTransaction.addThemeVariants(GridVariant.LUMO_ROW_STRIPES);

        this.layGridTransaction.add(gridTransaction);
        content5.add(this.layGridTransaction, gridTransaction);
    }

    protected Map<String, Object> createFilterParam(Query<?, Map<String, Object>> query) {
        Map<String, Object> props = new HashMap<>();
        if (query.getFilter().isPresent()) {
            props.putAll(query.getFilter().get());
        }
        if (query.getSortOrders() != null && !query.getSortOrders().isEmpty()) {
            QuerySortOrder querySortOrder = (QuerySortOrder) query.getSortOrders().get(0);
            props.put("sortOrder", querySortOrder.getDirection().toString());
            props.put("sortField", querySortOrder.getSorted());
        }
//			if (query.getLimit() > 0 && query.getLimit() < 2147483647) {
        props.put("limit", query.getLimit());
//			}

//			if (query.getOffset() > 0) {
        props.put("offset", query.getOffset());
        props.put("negativeBallStatus", "All");
//			}
        return props;
    }

    @SuppressWarnings("unchecked")
    public void refreshAll() {
        ((ConfigurableFilterDataProvider) gridData.getDataProvider()).refreshAll();
    }

    @SuppressWarnings("unchecked")
    public void refreshAllTransaction() {
        ((ConfigurableFilterDataProvider) gridTransaction.getDataProvider()).refreshAll();
    }

    public List<AccountModel> fetchData(Map<String, Object> filter) throws EndpointException, JsonProcessingException {
        return accountService.getAccounts(filter).getData();
    }

    public int countData(Map<String, Object> filter) throws EndpointException, JsonProcessingException {
        return accountService.getAccounts(filter).getRowCount().intValue();
    }


    public List<TransactionModel> fetchTransactionData(Map<String, Object> filter) throws EndpointException, JsonProcessingException {
        return transactionService.getTransactionList(filter).getData();
    }


    public int countTransactionData(Map<String, Object> filter) throws EndpointException, JsonProcessingException {
        return transactionService.getTransactionList(filter).getRowCount().intValue();
    }


}







