package com.daksa.web.ui.jsf;

import com.daksa.web.clientapi.AccountService;
import com.daksa.web.exception.EndpointException;
import com.daksa.web.model.AccountModel;
import com.daksa.web.model.UploadModel;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.vaadin.flow.component.upload.Upload;
import com.vaadin.flow.component.upload.receivers.MemoryBuffer;
import org.primefaces.PrimeFaces;
import org.primefaces.event.FileUploadEvent;
import org.primefaces.event.FilesUploadEvent;
import org.primefaces.model.file.UploadedFile;
import org.primefaces.model.file.UploadedFiles;
import org.primefaces.util.EscapeUtils;

import javax.faces.application.FacesMessage;
import javax.faces.bean.RequestScoped;
import javax.faces.context.FacesContext;
import javax.inject.Inject;
import javax.inject.Named;
import java.io.IOException;
import java.io.InputStream;

@Named
@RequestScoped

public class UploadView {

        private UploadedFile file;
        @Inject
        private AccountService accountService;


        public void handleFileUpload(FileUploadEvent event) throws IOException, EndpointException {


            InputStream inputStream = event.getFile().getInputStream();
            String resultOfInputStream="";
            resultOfInputStream = new String(inputStream.readAllBytes());
            UploadModel uploadModel = new UploadModel();
            uploadModel.setInputStream(resultOfInputStream);

            try {
                accountService.uploadAccount(uploadModel);
                FacesMessage message = new FacesMessage(FacesMessage.SEVERITY_INFO,"Upload success!", event.getFile().getFileName() + " is upload.");
                FacesContext.getCurrentInstance().addMessage(null, message);
            } catch (JsonProcessingException e) {
                e.printStackTrace();
                FacesMessage message = new FacesMessage(FacesMessage.SEVERITY_ERROR,"Upload failed!", "Please upload another file.");
                FacesContext.getCurrentInstance().addMessage(null, message);
            } catch (EndpointException e) {
                e.printStackTrace();
                FacesMessage message = new FacesMessage(FacesMessage.SEVERITY_ERROR,"Upload failed!", "Please upload another file.");
                FacesContext.getCurrentInstance().addMessage(null, message);
            }
        }


        public UploadedFile getFile() {
            return file;
        }

        public void setFile(UploadedFile file) {
            this.file = file;
        }




}
