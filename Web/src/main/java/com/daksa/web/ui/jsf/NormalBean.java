package com.daksa.web.ui.jsf;

import com.daksa.web.clientapi.AccountService;
import com.daksa.web.clientapi.TransactionService;
import com.daksa.web.exception.EndpointException;
import com.daksa.web.model.AccountModel;
import com.daksa.web.model.DailyTransactionModel;
import com.daksa.web.model.TransactionModel;
import com.daksa.web.model.TransferModel;
import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.core.JsonProcessingException;
import org.primefaces.PrimeFaces;
import org.primefaces.event.SelectEvent;

import javax.annotation.PostConstruct;
import javax.faces.application.FacesMessage;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.SessionScoped;
import javax.faces.bean.ViewScoped;
import javax.faces.context.FacesContext;
import javax.faces.event.ActionEvent;
import javax.faces.event.ValueChangeEvent;
import javax.inject.Inject;
import javax.inject.Named;
import java.math.BigDecimal;
import java.util.*;

@ManagedBean(name = "normal")
@ViewScoped
public class NormalBean{

    @Inject
    private AccountService accountService;
    @Inject
    private TransactionService transactionService;

    private String inputId;
    private String inputName;
    private String inputAddress;

    @JsonFormat(pattern = "dd-MM-yyyy")
    private Date inputBirthDate;

    private Boolean allowNegativeBallance;

    private List<AccountModel> allAccount;
    private List<TransactionModel>allTransaction;

    private String transferSrcId;
    private String transferDstId;
    private BigDecimal transferAmount;

    private String foundAccountName;

    private String selected;

    private String selectedOptionValue;
    private String allNegBallFilter;

    public String getAllNegBallFilter() {
        return allNegBallFilter;
    }

    public void setAllNegBallFilter(String allNegBallFilter) {
        this.allNegBallFilter = allNegBallFilter;
    }

    public String getSelectedOptionValue() {
        return selectedOptionValue;
    }

    public void setSelectedOptionValue(String selectedOptionValue) {
        this.selectedOptionValue = selectedOptionValue;
    }


    public void setSelected(String selected) {
        this.selected = selected;
    }

    public String getSelected() {
        return selected;
    }


    public void changeSelectedValue(){
        this.selected = this.selectedOptionValue;
        this.allNegBallFilter=this.selectedOptionValue;
    }


    public String getFoundAccountName() {
        return foundAccountName;
    }

    public void findNameByDstId() throws EndpointException, JsonProcessingException {
        Map<String,Object> filter = new HashMap<>();
        filter.put("limit",0);
        filter.put("offset",0);
        filter.put("id", transferDstId);
        filter.put("name",null);
        filter.put("negativeBallStatus",null);
        List<AccountModel> listFoundAccount= accountService.getAccounts(filter).getData();


        if(listFoundAccount.size()!=0){
            foundAccountName=listFoundAccount.get(0).getName();
        }
        else{
            foundAccountName="Sorry, destination account id not found..";

        }

    }

    public String getTransferSrcId() {
        return transferSrcId;
    }

    public void setTransferSrcId(String transferSrcId) {
        this.transferSrcId = transferSrcId;
    }


    public BigDecimal getTransferAmount() {
        return transferAmount;
    }

    public void setTransferAmount(BigDecimal transferAmount) {
        this.transferAmount = transferAmount;
    }

    public String getTransferDstId() {
        return transferDstId;
    }

    public void setTransferDstId(String transferDstId) {
        this.transferDstId = transferDstId;
    }

    public List<AccountModel> getAllAccount() throws EndpointException, JsonProcessingException {
        Map<String,Object> filter = new HashMap<>();
        filter.put("limit",100);
        filter.put("offset",0);
        filter.put("negativeBallStatus","All");

        this.allAccount= accountService.getAccounts(filter).getData();

        return allAccount;
    }

    public List<TransactionModel> getAllTransaction() throws EndpointException, JsonProcessingException {
        Map<String,Object> filter = new HashMap<>();
        filter.put("limit",100);
        filter.put("offset",0);
        this.allTransaction= transactionService.getTransactionList(filter).getData();
        return allTransaction;
    }

    public Boolean getAllowNegativeBallance() {
        return allowNegativeBallance;
    }

    public void setAllowNegativeBallance(Boolean allowNegativeBallance) {
        this.allowNegativeBallance = allowNegativeBallance;
    }

    public String getInputName() {
        return inputName;
    }

    public void setInputName(String inputName) {
        this.inputName = inputName;
    }

    public String getInputId() {
        return inputId;
    }

    public void setInputId(String inputId) {
        this.inputId = inputId;
    }

    public String getInputAddress() {
        return inputAddress;
    }

    public void setInputAddress(String inputAddress) {
        this.inputAddress = inputAddress;
    }

    public Date getInputBirthDate() {
        return inputBirthDate;
    }

    public void setInputBirthDate(Date inputBirthDate) {
        this.inputBirthDate = inputBirthDate;
    }

    public void submit(){
        AccountModel accountModel = new AccountModel();
        accountModel.setAccountId(getInputId());
        accountModel.setName(getInputName());
        accountModel.setAddress(getInputAddress());
        accountModel.setBirthdate(getInputBirthDate());
        accountModel.setAllowNegBall(getAllowNegativeBallance());

        try {
            accountService.addACcount(accountModel);
            FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_INFO, "Registration success",
                    "Registration Success!"));

        } catch (JsonProcessingException e) {
            e.printStackTrace();
        } catch (EndpointException e) {
            FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_ERROR, "Id has been used",
                    "ID has been used"));
        }
    }


    public void transfer(){
        TransferModel transferModel = new TransferModel();
        transferModel.setSrcAccountId(this.transferSrcId);
        transferModel.setAmount(this.transferAmount);
        transferModel.setDstAccountId(this.transferDstId);
        try {
            transactionService.transfer(transferModel);
            FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_INFO, "Transfer Success!",
                    "Transfer success!"));
        } catch (JsonProcessingException e) {

        } catch (EndpointException e) {
            FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_ERROR, "Account not found or not allow for negative ballance!",
                    "Account not found or not allow for negative ballance!"));

        }
    }




}