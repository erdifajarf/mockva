package com.daksa.web.ui.jsf;



import com.daksa.web.clientapi.TransactionService;
import com.daksa.web.exception.EndpointException;
import com.daksa.web.model.DailyTransactionModel;
import com.daksa.web.model.TransactionModel;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.vaadin.flow.component.charts.model.DataSeries;
import com.vaadin.flow.component.charts.model.DataSeriesItem;
import org.primefaces.event.ItemSelectEvent;


import org.primefaces.model.charts.ChartData;
import org.primefaces.model.charts.line.LineChartDataSet;
import org.primefaces.model.charts.line.LineChartModel;
import org.primefaces.model.charts.line.LineChartOptions;
import org.primefaces.model.charts.optionconfig.title.Title;


import javax.annotation.PostConstruct;
import javax.faces.application.FacesMessage;
import javax.faces.bean.RequestScoped;
import javax.faces.bean.ViewScoped;
import javax.faces.context.FacesContext;
import javax.inject.Inject;
import javax.inject.Named;
import java.io.Serializable;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.List;


@Named
@ViewScoped
    public class ChartJsView implements Serializable {
        private LineChartModel lineModel;

        @Inject
        private TransactionService transactionService;

        @PostConstruct
        public void init() throws EndpointException, JsonProcessingException {
            createLineModel();
        }

        public void createLineModel() throws EndpointException, JsonProcessingException {
            lineModel = new LineChartModel();
            ChartData data = new ChartData();

            LineChartDataSet dataSet = new LineChartDataSet();
            List<Object> values = new ArrayList<>();

            List<DailyTransactionModel> allTransaction=transactionService.getTransactionDailyReport();

            for (DailyTransactionModel item : allTransaction) {
                values.add(item.getTotalAmount());
            }

            dataSet.setData(values);
            dataSet.setFill(false);
            dataSet.setLabel("Total Amount");
            dataSet.setBorderColor("rgb(67, 103, 1247)");
            data.addChartDataSet(dataSet);

            DateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd");

            List<String> labels = new ArrayList<>();
            for (DailyTransactionModel item : allTransaction) {
                labels.add(dateFormat.format(item.getTransactionDate()));
            }

            data.setLabels(labels);


            //Options
            LineChartOptions options = new LineChartOptions();
            Title title = new Title();
            title.setDisplay(true);
            title.setText("Transaction Chart");
            options.setTitle(title);


            lineModel.setOptions(options);
            lineModel.setData(data);
        }

//        public void itemSelect(ItemSelectEvent event) {
//            FacesMessage msg = new FacesMessage(FacesMessage.SEVERITY_INFO, "Item selected",
//                    "Item Index: " + event.getItemIndex() + ", DataSet Index:" + event.getDataSetIndex());
//
//            FacesContext.getCurrentInstance().addMessage(null, msg);
//        }

        public LineChartModel getLineModel() {
            return lineModel;
        }

        public void setLineModel(LineChartModel lineModel) {
            this.lineModel = lineModel;
        }

    }

