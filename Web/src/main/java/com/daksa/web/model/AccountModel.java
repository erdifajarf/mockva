package com.daksa.web.model;

import com.fasterxml.jackson.annotation.JsonFormat;

import javax.persistence.Column;
import javax.persistence.Id;
import java.math.BigDecimal;
import java.util.Date;

/**
 * Example object.
 */

public class AccountModel {

    private String accountId;
    private String name;
    private String address;

    @JsonFormat(pattern = "dd-MM-yyyy")
    private Date birthdate;
    private BigDecimal balance;
    private Boolean allowNegBall;

    public String getAccountId() {
        return accountId;
    }

    public void setAccountId(String accountId) {
        this.accountId = accountId;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public Date getBirthdate() {
        return birthdate;
    }

    public void setBirthdate(Date birthdate) {
        this.birthdate = birthdate;
    }

    public BigDecimal getBalance() {
        return balance;
    }

    public void setBalance(BigDecimal balance) {
        this.balance = balance;
    }

    public Boolean getAllowNegBall() {
        return allowNegBall;
    }

    public void setAllowNegBall(Boolean allowNegBall) {
        this.allowNegBall = allowNegBall;
    }

    public AccountModel(String accountId, String name, String address, Date birthdate, BigDecimal balance, Boolean allowNegBall) {
        this.accountId = accountId;
        this.name = name;
        this.address = address;
        this.birthdate = birthdate;
        this.balance = balance;
        this.allowNegBall = allowNegBall;
    }

    public AccountModel() {

    }


}

