package com.daksa.web.model;

import com.fasterxml.jackson.annotation.JsonFormat;

import javax.persistence.Column;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import java.math.BigDecimal;
import java.util.Date;

public class TransactionModel {

    private String transactionId;
    private AccountModel srcAccountId;
    private AccountModel dstAccountId;
    @JsonFormat(pattern = "dd-MM-yyyy")
    private Date transactionTimeStamp;
    private BigDecimal amount;


    public TransactionModel(String transactionId, AccountModel srcAccountId, AccountModel dstAccountId, Date transactionTimeStamp, BigDecimal amount) {
        this.transactionId = transactionId;
        this.srcAccountId = srcAccountId;
        this.dstAccountId = dstAccountId;
        this.transactionTimeStamp = transactionTimeStamp;
        this.amount = amount;
    }

    public String getTransactionId() {
        return transactionId;
    }

    public void setTransactionId(String transactionId) {
        this.transactionId = transactionId;
    }

    public AccountModel getSrcAccount() {
        return srcAccountId;
    }

    public String getSrcAccountId() {
        return srcAccountId.getAccountId();
    }

    public void setSrcAccount(AccountModel srcAccountId) {
        this.srcAccountId = srcAccountId;
    }

    public AccountModel getDstAccount() {
        return dstAccountId;
    }

    public String getDstAccountId() {
        return dstAccountId.getAccountId();
    }

    public void setDstAccountId(AccountModel dstAccountId) {
        this.dstAccountId = dstAccountId;
    }

    public Date getTransactionTimeStamp() {
        return transactionTimeStamp;
    }

    public void setTransactionTimeStamp(Date transactionTimeStamp) {
        this.transactionTimeStamp = transactionTimeStamp;
    }

    public BigDecimal getAmount() {
        return amount;
    }

    public void setAmount(BigDecimal amount) {
        this.amount = amount;
    }

    public TransactionModel() {
    }



//    private String transactionId;
//    private String srcAccountId;
//    private String dstAccountId;
//    @JsonFormat(pattern = "dd-MM-yyyy")
//    private Date transactionTimeStamp;
//    private BigDecimal amount;
//
//    public TransactionModel() {
//    }
//
//    public TransactionModel(String transactionId, String srcAccountId, String dstAccountId, Date transactionTimeStamp, BigDecimal amount) {
//        this.transactionId = transactionId;
//        this.srcAccountId = srcAccountId;
//        this.dstAccountId = dstAccountId;
//        this.transactionTimeStamp = transactionTimeStamp;
//        this.amount = amount;
//    }
//
//    public String getTransactionId() {
//        return transactionId;
//    }
//
//    public void setTransactionId(String transactionId) {
//        this.transactionId = transactionId;
//    }
//
//    public String getSrcAccountId() {
//        return srcAccountId;
//    }
//
//    public void setSrcAccountId(String srcAccountId) {
//        this.srcAccountId = srcAccountId;
//    }
//
//    public String getDstAccountId() {
//        return dstAccountId;
//    }
//
//    public void setDstAccountId(String dstAccountId) {
//        this.dstAccountId = dstAccountId;
//    }
//
//    public Date getTransactionTimeStamp() {
//        return transactionTimeStamp;
//    }
//
//    public void setTransactionTimeStamp(Date transactionTimeStamp) {
//        this.transactionTimeStamp = transactionTimeStamp;
//    }
//
//    public BigDecimal getAmount() {
//        return amount;
//    }
//
//    public void setAmount(BigDecimal amount) {
//        this.amount = amount;
//    }
}
