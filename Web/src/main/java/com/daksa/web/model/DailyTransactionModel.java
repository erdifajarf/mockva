package com.daksa.web.model;

import com.fasterxml.jackson.annotation.JsonFormat;

import java.math.BigDecimal;
import java.util.Date;

public class DailyTransactionModel {
    @JsonFormat(pattern = "dd-MM-yyyy")
    private Date transactionDate;
    private BigDecimal totalAmount;

    public DailyTransactionModel() {
    }

    public DailyTransactionModel(Date transactionDate, BigDecimal totalAmount) {
        this.transactionDate = transactionDate;
        this.totalAmount = totalAmount;
    }

    public Date getTransactionDate() {
        return transactionDate;
    }

    public void setTransactionDate(Date transactionDate) {
        this.transactionDate = transactionDate;
    }

    public BigDecimal getTotalAmount() {
        return totalAmount;
    }

    public void setTotalAmount(BigDecimal totalAmount) {
        this.totalAmount = totalAmount;
    }
}
