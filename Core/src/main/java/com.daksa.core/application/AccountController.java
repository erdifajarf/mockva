package com.daksa.core.application;

import com.daksa.core.domain.Account;
import com.daksa.core.infrastructure.TableResult;
import com.daksa.core.model.AccountModel;
import com.daksa.core.model.UploadModel;
import com.daksa.core.repository.AccountRepository;
import com.daksa.core.services.UploadService;

import javax.faces.bean.RequestScoped;
import javax.inject.Inject;
import javax.transaction.Transactional;
import javax.ws.rs.*;
import javax.ws.rs.core.MediaType;
import java.io.IOException;
import java.math.BigDecimal;
import java.util.HashMap;
import java.util.Map;

@RequestScoped
@Path("account")
public class AccountController {
    @Inject
    private AccountRepository accountRepository;

    @Inject
    private UploadService uploadService;

    @GET
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.APPLICATION_JSON)
    public TableResult<Account> getAccount(@QueryParam("limit") int limit,
                                           @QueryParam("offset") int offset,
                                           @QueryParam("id") String id,
                                           @QueryParam("name") String name,
                                           @QueryParam("negativeBallStatus") String negativeBallStatus) {

        Map<String, Object> filter = new HashMap<>();
        filter.put("limit", limit);
        filter.put("offset", offset);
        filter.put("id", id);
        filter.put("name", name);
        filter.put("negativeBallStatus", negativeBallStatus);
        return this.accountRepository.getAccounts(filter);

    }


    @Transactional
    @POST
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.APPLICATION_JSON)
    public void registerAccount(AccountModel accountModel) throws IOException {
        Account account = new Account();
            account.setAccountId(accountModel.getAccountId());
            account.setName(accountModel.getName());
            account.setAddress(accountModel.getAddress());
            account.setBirthdate(accountModel.getBirthdate());
            account.setBalance(new BigDecimal(0.00));
            account.setAllowNegBall(accountModel.getAllowNegBall());
            this.accountRepository.add(account);
    }


    @Transactional
    @POST
    @Path("upload")
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.APPLICATION_JSON)
    public void uploadAccount(UploadModel uploadModel) {
        String[] specifiedLine = uploadModel.getInputStream().split("\\n");

        for (int i = 0; i < specifiedLine.length; i++) {
            Account account = uploadService.readFromUploadFile(specifiedLine[i]);
            this.accountRepository.add(account);
        }
    }


}
