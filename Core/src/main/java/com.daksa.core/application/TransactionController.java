package com.daksa.core.application;


import com.daksa.core.domain.Account;
import com.daksa.core.domain.Transaction;

import com.daksa.core.infrastructure.TableResult;
import com.daksa.core.model.AccountModel;
import com.daksa.core.model.DailyTransactionModel;
import com.daksa.core.model.TransactionModel;

import com.daksa.core.model.TransferModel;
import com.daksa.core.repository.AccountRepository;
import com.daksa.core.repository.TransactionRepository;
//import com.daksa.core.services.CalculateBallance;

import javax.faces.bean.RequestScoped;
import javax.inject.Inject;
import javax.transaction.Transactional;
import javax.ws.rs.*;
import javax.ws.rs.core.MediaType;
import java.math.BigDecimal;
import java.text.SimpleDateFormat;
import java.util.Date;

@RequestScoped
@Path("transaction")
public class TransactionController {

    @Inject
    private TransactionRepository transactionRepository;
    @Inject
    private AccountRepository accountRepository;


    @Transactional
    @Path("transfer")
    @POST
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.APPLICATION_JSON)
    public void transactionTransfer(TransferModel transferModel) throws Exception {
        Date currTimeonDate = new Date();
        Long currTime = currTimeonDate.getTime();

        Date timeStamp = new Date(System.currentTimeMillis());

        transferModel.setTransactionTimeStamp(timeStamp);
        transferModel.setTransactionId(currTime.toString() + ":" + transferModel.getSrcAccountId() + ":" + transferModel.getDstAccountId());

        Transaction transaction = new Transaction();
        Account accSrc = accountRepository.findAndLockById(transferModel.getSrcAccountId());
        Account accDst = accountRepository.findAndLockById(transferModel.getDstAccountId());

        if (!accSrc.getAllowNegBall()&&accSrc.getBalance().subtract(transferModel.getAmount()).doubleValue()<0) {
            throw new IllegalArgumentException("Account not allowed for negative ballance");
        }
        transaction.setTransactionId(transferModel.getTransactionId());
        transaction.setSrcAccountId(accSrc);
        transaction.setDstAccountId(accDst);
        transaction.setTransactionTimeStamp(transferModel.getTransactionTimeStamp());
        transaction.setAmount(transferModel.getAmount());

        if (!(transferModel.getAmount().doubleValue() > 0)) {
            throw new Exception("Transfer amount must greater than 0");
        }
        accSrc.setBalance(accSrc.getBalance().subtract(transferModel.getAmount()));
        accDst.setBalance(accDst.getBalance().add(transferModel.getAmount()));

        transactionRepository.addTransaction(transaction);
    }

    @GET
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.APPLICATION_JSON)
    public TableResult<Transaction> getTransactionList(@QueryParam("limit") int limit,
                                                       @QueryParam("offset") int offset,
                                                       @QueryParam("id") String id,
                                                       @QueryParam("srcAccountId") String srcAccountId,
                                                       @QueryParam("dstAccountId") String dstAccountId) {
        return this.transactionRepository.getTransactionList(limit, offset);
    }


    @GET
    @Path("daily")
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.APPLICATION_JSON)
    public TableResult<DailyTransactionModel> getTransactionDailyReport(@QueryParam("limit") int limit,
                                                                        @QueryParam("offset") int offset) {
        return this.transactionRepository.getTransactionDailyReport();
    }


}

