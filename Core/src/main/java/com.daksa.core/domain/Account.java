package com.daksa.core.domain;
import com.fasterxml.jackson.annotation.JsonFormat;

import javax.persistence.*;
import javax.xml.bind.annotation.XmlRootElement;
import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;
import java.util.HashSet;
import java.util.Set;


@Entity
@Table(name="account")
@NamedQueries({@NamedQuery(name="account.findId",query = "SELECT e FROM Account e WHERE LOWER(e.accountId) =:id"),
        @NamedQuery(name="account.getAccounts",query = "SELECT e FROM Account e ORDER BY e.accountId")})
public class Account {


    @Id
    private String accountId;

    @Column(name="name",nullable = false, length = 150)
    private String name;

    @Column(name="address",nullable = true)
    private String address;

    @JsonFormat(pattern = "dd-MM-yyyy")
    @Column(name="birthdate",nullable=true)
    private Date birthdate;

    @Column(name="balance",nullable = true)
    private BigDecimal balance;

    @Column(name="allow_negatif_balance",nullable = false)
    private Boolean allowNegBall;

    public Account() {
    }

    public String getAccountId() {
        return accountId;
    }

    public void setAccountId(String accountId) {
        this.accountId = accountId;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public Date getBirthdate() {
        return birthdate;
    }

    public void setBirthdate(Date birthdate) {
        this.birthdate = birthdate;
    }

    public BigDecimal getBalance() {
        return balance;
    }

    public void setBalance(BigDecimal balance) {
        this.balance = balance;
    }

    public Boolean getAllowNegBall() {
        return allowNegBall;
    }

    public void setAllowNegBall(Boolean allowNegBall) {
        this.allowNegBall = allowNegBall;
    }

    public Account(String accountId, String name, String address, Date birthdate, BigDecimal balance, Boolean allowNegBall) {
        this.accountId = accountId;
        this.name = name;
        this.address = address;
        this.birthdate = birthdate;
        this.balance = balance;
        this.allowNegBall = allowNegBall;
    }
}
