package com.daksa.core.domain;
import com.fasterxml.jackson.annotation.JsonFormat;

import javax.persistence.*;
import java.math.BigDecimal;
import java.util.Date;

@Entity
@Table(name="transaction")
@NamedQueries({@NamedQuery(name="transaction.getTransactionList",query = "SELECT e FROM Transaction e"),
@NamedQuery(name="transaction.getTransactionDailyReport", query="SELECT NEW com.daksa.core.model.DailyTransactionModel(e.transactionTimeStamp, " +
        "SUM(e.amount)) FROM Transaction e GROUP BY e.transactionTimeStamp ORDER BY e.transactionTimeStamp")})

public class Transaction {
    @Id
    private String transactionId;

    @ManyToOne
    @JoinColumn(name="src_account_id", nullable=true)
    private Account srcAccountId;

    @ManyToOne
    @JoinColumn(name="dst_account_id", nullable=true)
    private Account dstAccountId;

    @Temporal(TemporalType.DATE)
    @JsonFormat(pattern = "dd-MM-yyyy")
    @Column(name="transaction_time_stamp",nullable = true)
    private Date transactionTimeStamp;

    @Column(name="amount",nullable = false)
    private BigDecimal amount;


    public Transaction() {
    }

    public Transaction(String transactionId, Account srcAccountId, Account dstAccountId, Date transactionTimeStamp, BigDecimal amount) {
        this.transactionId = transactionId;
        this.srcAccountId = srcAccountId;
        this.dstAccountId = dstAccountId;
        this.transactionTimeStamp = transactionTimeStamp;
        this.amount = amount;
    }

    public String getTransactionId() {
        return transactionId;
    }

    public void setTransactionId(String transactionId) {
        this.transactionId = transactionId;
    }

    public Account getSrcAccountId() {
        return srcAccountId;
    }

    public void setSrcAccountId(Account srcAccountId) {
        this.srcAccountId = srcAccountId;
    }

    public Account getDstAccountId() {
        return dstAccountId;
    }

    public void setDstAccountId(Account dstAccountId) {
        this.dstAccountId = dstAccountId;
    }

    public Date getTransactionTimeStamp() {
        return transactionTimeStamp;
    }

    public void setTransactionTimeStamp(Date transactionTimeStamp) {
        this.transactionTimeStamp = transactionTimeStamp;
    }

    public BigDecimal getAmount() {
        return amount;
    }

    public void setAmount(BigDecimal amount) {
        this.amount = amount;
    }





}
