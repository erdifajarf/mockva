package com.daksa.core.services;

import com.daksa.core.domain.Account;

import javax.enterprise.context.Dependent;
import java.io.InputStream;
import java.math.BigDecimal;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

@Dependent
public class UploadService {

    public Account readFromUploadFile(String resultOfInputStream) {
        Account account = new Account();

        String id = resultOfInputStream.substring(0, 16).trim();

        String nameLength = resultOfInputStream.substring(16, 18);
        int nameLengthOnInt = Integer.parseInt(nameLength);
        int idxEndName = 18 + nameLengthOnInt;
        String name = resultOfInputStream.substring(18, idxEndName);

        int addressEndLength = idxEndName + 2;
        String addressLength = resultOfInputStream.substring(idxEndName, addressEndLength);
        int addressLengthOnInt = Integer.parseInt(addressLength);
        int addressEnd = addressEndLength + addressLengthOnInt;

        String address = resultOfInputStream.substring(addressEndLength, addressEnd);

        int endDate = addressEnd + 8;
        String dateOnString = resultOfInputStream.substring(addressEnd, endDate);
        String year = dateOnString.substring(0, 4);
        String month = dateOnString.substring(4, 6);
        String day = dateOnString.substring(6, 8);
        String dateTemp = month + "/" + day + "/" + year;
        Date birthdate = new Date();
        try {
            birthdate = new SimpleDateFormat("MM-dd-yyyy").parse(dateTemp);
        } catch (ParseException e) {
            e.printStackTrace();
        }

        int endAllowNegativeBallance = endDate + 1;
        String allowNegativeBallanceOnString = resultOfInputStream.substring(endDate, endAllowNegativeBallance);
        Boolean allowNegativeBallance;

        if (allowNegativeBallanceOnString.equalsIgnoreCase("Y")) {
            allowNegativeBallance = true;
        } else {
            allowNegativeBallance = false;
        }
        account.setAccountId(id);
        account.setName(name);
        account.setAddress(address);
        account.setBirthdate(birthdate);
        account.setBalance(new BigDecimal(0.00));
        account.setAllowNegBall(allowNegativeBallance);

        return account;
    }
}
