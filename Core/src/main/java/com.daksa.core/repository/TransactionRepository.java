package com.daksa.core.repository;


import com.daksa.core.domain.Account;
import com.daksa.core.domain.Transaction;
import com.daksa.core.infrastructure.TableResult;
import com.daksa.core.model.AccountModel;
import com.daksa.core.model.DailyTransactionModel;
import com.daksa.core.model.TransactionModel;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.boot.registry.StandardServiceRegistryBuilder;

import javax.ejb.Stateless;
import javax.enterprise.context.Dependent;
import javax.imageio.spi.ServiceRegistry;
import javax.inject.Inject;
import javax.persistence.*;
import javax.transaction.Transactional;
import javax.ws.rs.WebApplicationException;
import java.lang.module.Configuration;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

@Dependent
public class TransactionRepository {

    @Inject
    private EntityManager entityManager;


    public Transaction addTransaction(Transaction transaction) {
        entityManager.persist(transaction);
        return transaction;
    }

    public TableResult<Transaction> getTransactionList(int limit, int offset) {
        TypedQuery<Transaction> query = entityManager.createNamedQuery("transaction.getTransactionList",Transaction.class);

        if(limit!=0)
        {
            query.setMaxResults(limit);
            query.setFirstResult(offset);
        }

        TableResult<Transaction> tableResult = new TableResult<>();
        tableResult.setData(query.getResultList());
        tableResult.setRowCount((long) query.getResultList().size());
        return tableResult;
    }


    public TableResult<DailyTransactionModel> getTransactionDailyReport() {
        TypedQuery<DailyTransactionModel> query = entityManager.createNamedQuery("transaction.getTransactionDailyReport",
                DailyTransactionModel.class);

        TableResult<DailyTransactionModel>tableResult = new TableResult<>();
        tableResult.setData(query.getResultList());
        tableResult.setRowCount((long) query.getResultList().size());

        return tableResult;
    }




}




