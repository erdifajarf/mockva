package com.daksa.core.repository;



import com.daksa.core.domain.Account;
import com.daksa.core.infrastructure.TableResult;
//import com.daksa.core.services.CalculateBallance;

import javax.enterprise.context.Dependent;
import javax.inject.Inject;
import javax.persistence.*;
import java.math.BigDecimal;
import java.util.HashMap;
import java.util.Map;

@Dependent
public class AccountRepository {

    @Inject
    private EntityManager entityManager;
    private Query query;

    public Account add(Account account){
        entityManager.persist(account);
        return account;
    }

    public TableResult<Account> getAccounts(Map<String,Object> filter) {
        int limit = (Integer)filter.get("limit");
        int offset = (Integer)filter.get("offset");
        String id = filter.get("id").toString();
        String name = filter.get("name").toString();
        String negativeBallStatus = filter.get("negativeBallStatus").toString();

        StringBuilder queryString = new StringBuilder();
        queryString.append("SELECT e FROM Account e WHERE true=true");


        if(id!=null && !id.isEmpty() && !negativeBallStatus.equals("")){
            queryString.append(" AND LOWER(e.accountId) LIKE CONCAT('%',:id,'%')");
        }

        if(negativeBallStatus.isEmpty()) {
            queryString.append(" AND LOWER(e.accountId) =:id");
        }

        if(name!=null && !name.isEmpty()){
            queryString.append(" AND LOWER(e.name) LIKE CONCAT('%',:name,'%')");
        }
        if(negativeBallStatus!=null && !negativeBallStatus.isEmpty() && !negativeBallStatus.equalsIgnoreCase("All")){
            queryString.append(" AND e.allowNegBall =:negativeBallStatus");
        }

        queryString.append(" ORDER BY e.accountId");


        TypedQuery<Account> query = this.entityManager.createQuery(queryString.toString(),Account.class);
        if(id!=null && !id.isEmpty()){
            query.setParameter("id",id);
        }
        if(negativeBallStatus.equalsIgnoreCase("")){
            query.setParameter("id",id);
        }
        if(name!=null && !name.isEmpty()){
            query.setParameter("name",name);
        }
        if(negativeBallStatus!=null && !negativeBallStatus.isEmpty() && !negativeBallStatus.equalsIgnoreCase("All")){
            query.setParameter("negativeBallStatus",Boolean.parseBoolean(negativeBallStatus));
        }

        if((Integer)filter.get("limit")!=0)
        {
            query.setMaxResults(limit);
            query.setFirstResult(offset);
        }

        TableResult<Account> tableResult = new TableResult<>();
        tableResult.setData(query.getResultList());
        tableResult.setRowCount((long) query.getResultList().size());
        return tableResult;
    }


    public Account findAndLockById(String id) {
        Account accountResult = entityManager.find(Account.class, id,LockModeType.PESSIMISTIC_READ);
        return accountResult;
    }




}







