package com.daksa.core.model;

import com.fasterxml.jackson.annotation.JsonFormat;

import java.math.BigDecimal;
import java.util.Date;

public class UploadModel {

    private String accountId;
    private String name;
    private String address;

    @JsonFormat(pattern = "dd-MM-yyyy")
    private Date birthdate;
    private BigDecimal balance;
    private Boolean allowNegBall;

    private String inputStream;

    public UploadModel() {
    }

    public UploadModel(String accountId, String name, String address, Date birthdate, BigDecimal balance, Boolean allowNegBall, String inputStream) {
        this.accountId = accountId;
        this.name = name;
        this.address = address;
        this.birthdate = birthdate;
        this.balance = balance;
        this.allowNegBall = allowNegBall;
        this.inputStream = inputStream;
    }

    public String getAccountId() {
        return accountId;
    }

    public void setAccountId(String accountId) {
        this.accountId = accountId;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public Date getBirthdate() {
        return birthdate;
    }

    public void setBirthdate(Date birthdate) {
        this.birthdate = birthdate;
    }

    public BigDecimal getBalance() {
        return balance;
    }

    public void setBalance(BigDecimal balance) {
        this.balance = balance;
    }

    public Boolean getAllowNegBall() {
        return allowNegBall;
    }

    public void setAllowNegBall(Boolean allowNegBall) {
        this.allowNegBall = allowNegBall;
    }

    public String getInputStream() {
        return inputStream;
    }

    public void setInputStream(String inputStream) {
        this.inputStream = inputStream;
    }
}
