package com.daksa.core.model;

import com.daksa.core.domain.Account;
import com.fasterxml.jackson.annotation.JsonFormat;

import javax.persistence.Column;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import java.math.BigDecimal;
import java.util.Date;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

@JsonIgnoreProperties(ignoreUnknown = true)
public class TransactionModel {

    private String transactionId;
    private Account srcAccountId;
    private Account dstAccountId;
    @JsonFormat(pattern = "dd-MM-yyyy")
    private Date transactionTimeStamp;
    private BigDecimal amount;


    public TransactionModel(String transactionId, Account srcAccountId, Account dstAccountId, Date transactionTimeStamp, BigDecimal amount) {
        this.transactionId = transactionId;
        this.srcAccountId = srcAccountId;
        this.dstAccountId = dstAccountId;
        this.transactionTimeStamp = transactionTimeStamp;
        this.amount = amount;
    }

    public TransactionModel() {
    }

    public String getTransactionId() {
        return transactionId;
    }

    public void setTransactionId(String transactionId) {
        this.transactionId = transactionId;
    }

    public Account getSrcAccountId() {
        return srcAccountId;
    }

    public void setSrcAccountId(Account srcAccountId) {
        this.srcAccountId = srcAccountId;
    }

    public Account getDstAccountId() {
        return dstAccountId;
    }

    public void setDstAccountId(Account dstAccountId) {
        this.dstAccountId = dstAccountId;
    }

    public Date getTransactionTimeStamp() {
        return transactionTimeStamp;
    }

    public void setTransactionTimeStamp(Date transactionTimeStamp) {
        this.transactionTimeStamp = transactionTimeStamp;
    }

    public BigDecimal getAmount() {
        return amount;
    }

    public void setAmount(BigDecimal amount) {
        this.amount = amount;
    }




//    private String transactionId;
//    private String srcAccountId;
//    private String dstAccountId;
//    @JsonFormat(pattern = "dd-MM-yyyy")
//    private Date transactionTimeStamp;
//    private BigDecimal amount;
//
//    public TransactionModel() {
//    }
//
//    public TransactionModel(String transactionId, String srcAccountId, String dstAccountId, Date transactionTimeStamp, BigDecimal amount) {
//        this.transactionId = transactionId;
//        this.srcAccountId = srcAccountId;
//        this.dstAccountId = dstAccountId;
//        this.transactionTimeStamp = transactionTimeStamp;
//        this.amount = amount;
//    }
//
//    public String getTransactionId() {
//        return transactionId;
//    }
//
//    public void setTransactionId(String transactionId) {
//        this.transactionId = transactionId;
//    }
//
//    public String getSrcAccountId() {
//        return srcAccountId;
//    }
//
//    public void setSrcAccountId(String srcAccountId) {
//        this.srcAccountId = srcAccountId;
//    }
//
//    public String getDstAccountId() {
//        return dstAccountId;
//    }
//
//    public void setDstAccountId(String dstAccountId) {
//        this.dstAccountId = dstAccountId;
//    }
//
//    public Date getTransactionTimeStamp() {
//        return transactionTimeStamp;
//    }
//
//    public void setTransactionTimeStamp(Date transactionTimeStamp) {
//        this.transactionTimeStamp = transactionTimeStamp;
//    }
//
//    public BigDecimal getAmount() {
//        return amount;
//    }
//
//    public void setAmount(BigDecimal amount) {
//        this.amount = amount;
//    }
}
