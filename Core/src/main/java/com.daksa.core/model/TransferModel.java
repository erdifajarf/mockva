package com.daksa.core.model;

import com.daksa.core.domain.Account;
import com.fasterxml.jackson.annotation.JsonFormat;

import java.math.BigDecimal;
import java.util.Date;

public class TransferModel {
    private String transactionId;
    private String srcAccountId;
    private String dstAccountId;
    @JsonFormat(pattern = "dd-MM-yyyy")
    private Date transactionTimeStamp;
    private BigDecimal amount;

    public TransferModel(String transactionId, String srcAccountId, String dstAccountId, Date transactionTimeStamp, BigDecimal amount) {
        this.transactionId = transactionId;
        this.srcAccountId = srcAccountId;
        this.dstAccountId = dstAccountId;
        this.transactionTimeStamp = transactionTimeStamp;
        this.amount = amount;
    }

    public String getTransactionId() {
        return transactionId;
    }

    public void setTransactionId(String transactionId) {
        this.transactionId = transactionId;
    }

    public String getSrcAccountId() {
        return srcAccountId;
    }

    public void setSrcAccountId(String srcAccountId) {
        this.srcAccountId = srcAccountId;
    }

    public String getDstAccountId() {
        return dstAccountId;
    }

    public void setDstAccountId(String dstAccountId) {
        this.dstAccountId = dstAccountId;
    }

    public Date getTransactionTimeStamp() {
        return transactionTimeStamp;
    }

    public void setTransactionTimeStamp(Date transactionTimeStamp) {
        this.transactionTimeStamp = transactionTimeStamp;
    }

    public BigDecimal getAmount() {
        return amount;
    }

    public void setAmount(BigDecimal amount) {
        this.amount = amount;
    }

    public TransferModel() {
    }
}
