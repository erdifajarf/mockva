package com.daksa.core.repository;

import com.daksa.core.application.AccountController;
import com.daksa.core.domain.Account;
import com.daksa.core.infrastructure.TableResult;
//import com.daksa.core.services.CalculateBallance;
import com.daksa.core.model.AccountModel;
import com.daksa.core.model.DailyTransactionModel;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.MockitoJUnitRunner;
import org.mockito.junit.jupiter.MockitoExtension;

import javax.persistence.EntityManager;
import javax.persistence.LockModeType;
import javax.persistence.Query;
import javax.persistence.TypedQuery;
import java.math.BigDecimal;
import java.util.*;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.Mockito.*;


@ExtendWith(MockitoExtension.class)
class AccountRepositoryTest {
    @InjectMocks
    private AccountRepository accountRepository;
    @Mock
    private EntityManager entityManager;


    @Test
    void getAccounts() {
        List<Account> listAccount = new ArrayList<>();
        Account accountTest = new Account();
        accountTest.setAccountId("123");
        accountTest.setName("Erd");
        accountTest.setAllowNegBall(false);
        listAccount.add(accountTest);

        Map<String, Object> filter = new HashMap<>();
        filter.put("limit",0);
        filter.put("offset",0);
        filter.put("id", "1");
        filter.put("name","Erdi");
        filter.put("negativeBallStatus","true");


        String queryString = "SELECT e FROM Account e WHERE true=true AND LOWER(e.accountId) LIKE CONCAT('%',:id,'%') AND LOWER(e.name) LIKE CONCAT('%',:name,'%') AND e.allowNegBall =:negativeBallStatus ORDER BY e.accountId";

        TypedQuery<Account> typedQuery = mock(TypedQuery.class);
        when(entityManager.createQuery(queryString.toString(), Account.class)).thenReturn(typedQuery);


        accountRepository.getAccounts(filter);

        TableResult<Account> result= new TableResult<>();
        result.setData(listAccount);
        verify(entityManager).createQuery(queryString, Account.class);


        assertFalse(result.getData().isEmpty());
        assertEquals(0,filter.get("limit"));
        assertEquals("1",filter.get("id"));
    }

    @Test
    void add() {
        Account accountTest = new Account();
        accountTest.setAccountId("123");
        accountTest.setName("Dippa");
        Date birthdate = new Date();
        accountTest.setBirthdate(birthdate);
        accountRepository.add(accountTest);

        verify(entityManager).persist(any(Account.class));
        assertEquals("123", accountTest.getAccountId());
        assertEquals("Dippa", accountTest.getName());
    }


    @Test
    void findAndLockById() {
        String id = "123";

        List<Account> listAccount = new ArrayList<>();
        Account accountTest = new Account();
        accountTest.setAccountId("123");
        accountTest.setName("Erd");
        accountTest.setAllowNegBall(false);
        listAccount.add(accountTest);

        accountRepository.findAndLockById(accountTest.getAccountId());

        verify(entityManager).find(Account.class, id, LockModeType.PESSIMISTIC_READ);

        assertEquals(id, accountTest.getAccountId());
    }


}