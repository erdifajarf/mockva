package com.daksa.core.repository;

import com.daksa.core.domain.Account;
import com.daksa.core.domain.Transaction;
import com.daksa.core.infrastructure.TableResult;
import com.daksa.core.model.DailyTransactionModel;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

import javax.inject.Inject;
import javax.persistence.EntityManager;
import javax.persistence.TypedQuery;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;

import static org.junit.jupiter.api.Assertions.*;
import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.Mockito.*;
@ExtendWith(MockitoExtension.class)
class TransactionRepositoryTest {
    @InjectMocks
    private TransactionRepository transactionRepository;

    @Mock
    private EntityManager entityManager;

    @Test
    void addTransaction() {
        Account accountSrc= new Account();
        accountSrc.setAccountId("1");

        Account accountDst = new Account();
        accountDst.setAccountId("2");

        Transaction transaction = new Transaction();
        transaction.setTransactionId("1234567:1:2");
        transaction.setSrcAccountId(accountSrc);
        transaction.setDstAccountId(accountDst);
        transaction.setAmount(new BigDecimal(100.00));

        transactionRepository.addTransaction(transaction);

        verify(entityManager).persist(any(Transaction.class));
        assertEquals("1234567:1:2", transaction.getTransactionId());
        assertEquals("1", transaction.getSrcAccountId().getAccountId());
        assertEquals("2", transaction.getDstAccountId().getAccountId());

    }



    @Test
    void getTransactionList() {
        TypedQuery<Transaction> typedQuery = mock(TypedQuery.class);
        when(entityManager.createNamedQuery("transaction.getTransactionList",Transaction.class)).thenReturn(typedQuery);

        TableResult<Transaction> tableResult= new TableResult<>();
        List<Transaction> transactionList = new ArrayList<>();
        Transaction t1= new Transaction();
        Transaction t2= new Transaction();
        transactionList.add(t1);
        transactionList.add(t2);
        tableResult.setData(transactionList);

        transactionRepository.getTransactionList(1,0);

        verify(entityManager).createNamedQuery("transaction.getTransactionList",Transaction.class);

        assertFalse(tableResult.getData().isEmpty());
        assertEquals(2,tableResult.getData().size());

    }

    @Test
    void getTransactionDailyReport() {
        TypedQuery<DailyTransactionModel> typedQuery = mock(TypedQuery.class);
        when(entityManager.createNamedQuery("transaction.getTransactionDailyReport",DailyTransactionModel.class)).thenReturn(typedQuery);

        TableResult<DailyTransactionModel>tableResult = new TableResult<>();
        List<DailyTransactionModel>dailyTransactionModelList = new ArrayList<>();
        DailyTransactionModel DailyTransactionModel1= new DailyTransactionModel();
        dailyTransactionModelList.add(DailyTransactionModel1);

        tableResult.setData(dailyTransactionModelList);

        transactionRepository.getTransactionDailyReport();
        verify(entityManager).createNamedQuery("transaction.getTransactionDailyReport",DailyTransactionModel.class);


        assertEquals(1,tableResult.getData().size());

        /////////TERUS TYPED QUERYNYA BUAT APA?//////////
    }
}