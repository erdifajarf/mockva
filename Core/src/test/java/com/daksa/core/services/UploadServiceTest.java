package com.daksa.core.services;

import org.apache.log4j.BasicConfigurator;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.junit.jupiter.MockitoExtension;

import javax.inject.Inject;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.Mockito.*;

@ExtendWith(MockitoExtension.class)
class UploadServiceTest {

    @InjectMocks
    private UploadService uploadService;

    @Test
    void readFromUploadFile() {

        uploadService.readFromUploadFile("123456789123456705Daksa16Bandung Jl Windu20210101Y");

    }

    @Test
    void readFromUploadFileFailed() {
        assertThrows(IndexOutOfBoundsException.class, () -> {
            uploadService.readFromUploadFile("asd asd asd");
        });

        assertThrows(IndexOutOfBoundsException.class, () -> {
            uploadService.readFromUploadFile("asd");
        });
    }
}