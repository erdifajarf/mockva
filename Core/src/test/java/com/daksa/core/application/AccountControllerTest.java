package com.daksa.core.application;

import com.daksa.core.domain.Account;
import com.daksa.core.infrastructure.TableResult;
import com.daksa.core.model.AccountModel;
import com.daksa.core.model.UploadModel;
import com.daksa.core.repository.AccountRepository;
import com.daksa.core.services.UploadService;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

import java.io.IOException;
import java.math.BigDecimal;
import java.util.*;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.Mockito.*;

@ExtendWith(MockitoExtension.class)
class AccountControllerTest {

    @InjectMocks
    private AccountController accountController;

    @Mock
    private AccountRepository accountRepository;

    @Mock
    private UploadService uploadService;

    @Test
    void getAccount() {
        TableResult<Account> tableResult = new TableResult();
        List<Account> accountList = new ArrayList<>();
        Account account = new Account();
        account.setAccountId("1");
        account.setName("Erdi");
        account.setAllowNegBall(true);
        accountList.add(account);
        tableResult.setData(accountList);

        Map<String,Object> filter = new HashMap();
        filter.put("limit", 1);
        filter.put("offset", 0);
        filter.put("id", "1");
        filter.put("name", "Erdi");
        filter.put("negativeBallStatus", "All");

        when(accountRepository.getAccounts(filter)).thenReturn(tableResult);

        accountController.getAccount(1,0,"1","Erdi","All");

        verify(accountRepository).getAccounts(filter);

        assertFalse(tableResult.getData().isEmpty());
        assertFalse(filter.isEmpty());
        assertEquals("1",filter.get("limit").toString());

    }


    @Test
    void registerAccount() throws IOException {
        AccountModel accountModel = new AccountModel();
        accountModel.setAccountId("1");
        accountModel.setName("Erdi");
        accountModel.setAddress("Jl nias");
        accountModel.setBalance(new BigDecimal(0.00));
        accountModel.setAllowNegBall(true);
        Date birthdate = new Date();
        accountModel.setBirthdate(birthdate);


        Account account= new Account();
        account.setAccountId("1");

        when(accountRepository.add(any(Account.class))).thenReturn(account);/////////////////KOK GA JALAN

        accountController.registerAccount(accountModel);

        verify(accountRepository).add(any(Account.class)); /////////////////KOK GA JALAN

        assertEquals(accountModel.getAccountId(),account.getAccountId());
    }


    @Test
    void uploadAccount() {
        UploadModel uploadModel = new UploadModel();
        uploadModel.setInputStream("123456789123456705Daksa16Bandung Jl Windu20210101Y");

        Account accountResult = new Account();
        when(uploadService.readFromUploadFile(uploadModel.getInputStream())).thenReturn(accountResult);
        when(accountRepository.add(accountResult)).thenReturn(accountResult);
        accountController.uploadAccount(uploadModel);

        verify(uploadService).readFromUploadFile(any(String.class));
        verify(accountRepository).add(any(Account.class));

    }
}