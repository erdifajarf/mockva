package com.daksa.core.application;

import com.daksa.core.domain.Account;
import com.daksa.core.domain.Transaction;
import com.daksa.core.infrastructure.TableResult;
import com.daksa.core.model.DailyTransactionModel;
import com.daksa.core.model.TransferModel;
import com.daksa.core.repository.AccountRepository;
import com.daksa.core.repository.TransactionRepository;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.Mockito.*;

@ExtendWith(MockitoExtension.class)


class TransactionControllerTest {

    @InjectMocks
    private TransactionController transactionController;
    @Mock
    private TransactionRepository transactionRepository;
    @Mock
    private AccountRepository accountRepository;



    @Test
    void transactionTransfer() throws Exception {
        TransferModel transferModel = new TransferModel();
        transferModel.setAmount(new BigDecimal(100.00));
        transferModel.setSrcAccountId("1");
        transferModel.setDstAccountId("2");

        Account accountSrc = new Account();
        accountSrc.setAccountId("1");
        accountSrc.setName("Erdi");
        accountSrc.setAllowNegBall(true);
        accountSrc.setBalance(new BigDecimal(0.00));
        when(accountRepository.findAndLockById(transferModel.getSrcAccountId())).thenReturn(accountSrc);

        Account accountDst = new Account();
        accountDst.setAccountId("2");
        accountDst.setName("Evita");
        accountDst.setAllowNegBall(true);
        accountDst.setBalance(new BigDecimal(0.00));

        when(accountRepository.findAndLockById(transferModel.getDstAccountId())).thenReturn(accountDst);

        transactionController.transactionTransfer(transferModel);

        verify(accountRepository).findAndLockById(transferModel.getSrcAccountId());
        verify(accountRepository).findAndLockById(transferModel.getDstAccountId());

        assertEquals(new BigDecimal(-100.00),accountSrc.getBalance());
        assertEquals(new BigDecimal(100.00),accountDst.getBalance());
    }

    @Test
    void getTransactionList() {
        TableResult<Transaction> tableResult = new TableResult<>();

        List<Transaction>transactionList = new ArrayList<>();
        Transaction transaction1= new Transaction();
        Transaction transaction2= new Transaction();

        transactionList.add(transaction1);
        transactionList.add(transaction2);

        tableResult.setData(transactionList);

        when(transactionRepository.getTransactionList(1,0)).thenReturn(tableResult);

        transactionController.getTransactionList(1,0,null,null,null);

        verify(transactionRepository).getTransactionList(1,0);

        assertEquals(2,transactionList.size());
    }

    @Test
    void getTransactionDailyReport() {
        TableResult<DailyTransactionModel> tableResult = new TableResult<>();
        List<DailyTransactionModel>dailyTransactionModelList = new ArrayList<>();
        DailyTransactionModel dailyTransactionModel1= new DailyTransactionModel();
        DailyTransactionModel dailyTransactionModel2= new DailyTransactionModel();

        dailyTransactionModelList.add(dailyTransactionModel1);
        dailyTransactionModelList.add(dailyTransactionModel2);

        tableResult.setData(dailyTransactionModelList);

        when(transactionRepository.getTransactionDailyReport()).thenReturn(tableResult);

        transactionController.getTransactionDailyReport(1,0);

        verify(transactionRepository).getTransactionDailyReport();

        assertEquals(2,dailyTransactionModelList.size());


    }
}